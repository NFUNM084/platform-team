# 平台经济案例研究
### 研究对象：今日头条  
**产品背景：** 移动互联网不断发展，人们对于随时随地阅读信息资讯的需求增加，新闻APP等大量出现，人们的选择也不断变多，而今日头条在多年的发展下成为该领域的佼佼者。  
**产品简介：** 今日头条是一个信息类的平台，是一个基于数据挖掘的推荐引擎产品，为用户推荐信息。 **口号为** “你关心的，才是头条。”
### A-1 平台理论
#### 邱达利设计的“平台画布”
#### 平台画布最上层：参与者+价值单元+过滤器=核心交互  
**参与者：**
- 生产者：提供原创文章的特色功能头条号作者，提供信息资讯的各大新闻资讯类网站。
- 消费者：希望能够及时获取最新以及感兴趣信息的读者，根据数据显示，用今日头条的男性用户多于女性用户。
  
**价值单元：**  
- 一次成功被用户点击阅读的资讯推送。  

 **过滤器** ：
- 今日头条作为一个互联网平台，所起的关键作用就是“过滤器”，它通过算法推荐系统，将平台中原创的文章与集合的资讯跟对某些内容感兴趣的用户进行匹配，即把分析得出用户可能感兴趣的文章推送给用户；同时今日头条的分类功能也帮助用户获取更具体细化的内容类别。  

#### 平台画布第二层：工具与服务
- 创造的工具和服务：特色功能头条号
- 策展与个性化的工具与服务：首页“推荐”版块，提示刷新交互；首页“不感兴趣”功能，“关注”模块 
 
   -  **策展：** 对用户使用平台、参与的活动，及用户连接进行过滤、控制和限制的过程。
  
  如果出现今日头条推送内容与用户兴趣点不符的情况，就可能导致阅读量和点击率大幅下降，用户将离开该平台，由于消极网络效应的影响，头条号的内容生产者也会随之离开平台。今日头条的特性决定了它的策展是将平台上的大量信息精准推送给符合喜好的用户，主要体现在首页的“推荐”版块上。
- 消费的工具与服务：头条号盈利，付费阅读功能

#### 平台画布第三层：货币与获取
 **货币** ：
- 现金货币：付费阅读产生的线上支付费用
- 社交货币：关注、点赞、评论、分享等  

 **获取** ：
- 变现方式：以精准的信息流广告为主，依据个性化推荐机制精准地将广告分发给用户；和电商合作，提供流量入口，通过成交额获取分成。

#### 今日头条是如何吸引用户的？  
今日头条最开始是设计吸引某一类用户，头条的内容来自于其他门户新闻的汇总。不管什么引擎，它的首要工作都是要通过爬虫，积累足够多的数据样本。头条对门户加推荐引擎的模式，用户点击新闻标题后，会跳转到新闻门户的原网页，也就是说它初期并没有在生产内容，而是直接抓取了其他网站的内容，旨在吸引对兴趣推荐有需求的用户。而今日头条诞生时间正好与移动互联网大发展的时间相吻合，充分享受了移动互联网初期的红利，获得了病毒式增长。  
而今日头条保持用户黏性的方法也很简单，他们使用了多用户反馈回路，不断优化算法推荐，提高内容质量，把内容不断地推送给合适的用户，而用户的点击、阅读量以及评论就是对于生产者的反馈，从而使得用户反复使用今日头条，强化用户黏性。

#### 衡量指标
- 今日头条于2012年3月创建，2012年8月发布第一个版本，已经有7年历史，截至2019年3月底，今日头条在月活数量行业领先，月活用户数量为19933.9万人，产品目前已经处于 **成熟阶段** 。除了不断优化核心交互的体验之外，也开始注重创新，推出除了核心交互之外新的交互功能，创造出新类型的价值单元，如收购相机appFaceu、收购音乐短视频平台Musical.ly、加入了“保险”板块，推出一款联合互联网保险公司泰康在线开发的医疗险产品——“合家保·全家共享健康保障计划”等，去逐步构建一个属于今日头条的生态链。

#### A-2 平台中国案例分析成果  
- **反设计原则：** 智能化设计是建立和维持一个成功的平台必不可少的，但是今日头条完全依赖于算法推荐，难免会产生个性化推荐过度的情形，基本扼杀了用户获取其他信息的可能，没有留给偶然和意外更多的空间，因此今日头条在反设计原则方面是存在缺失的。
- **负外部效应和政策：** 今日头条最核心的交互就是根据用户画像，通过算法推荐系统，为用户推荐感兴趣的信息资讯。**但同时也存在着很致命的问题：负外部效应和来自国家版权局的压力。**   
    - 对于今日头条而言，负外部效应指的是推送内容的侵权和非法问题。今日头条被多次曝出盗取其他新闻网站内容，侵犯所属作品的信息网络传播权等问题，这样会把损失转嫁给那些和今日头条无关的信息门户网站，他们平白无故地被人截取流量，盗取内容，影响了他们的传播，这很容易引起大众对今日头条的质疑和不满，也是国家版权局会向今日头条施加压力的原因。
负外部效应是引起政府干预的理由之一，由于今日头条的侵权以及内容不良等行为，以及平台强大的影响力，会造成极大的负面影响，因此需要通过 **法律** 这个 **治理工具** 进行管制和治理，2018年3月30日，北京市工商行政管理局海淀分局对今日头条做出行政处罚，没收广告费约23.6万元，并处广告费用3倍的罚款，共计罚没约94.4万元。2018年4月4日，广电总局对今日头条主要负责人进行了约谈，并要求全面整改。这些都是来自国家版权局、广电总局以及工商行政管理局等部门对于今日头条这个平台的 **监管** 。
    - 随着今日头条的不断成熟发展，今日头条更加注重版权维护，至今仍可看到网页版右上角有明确的“侵权投诉”功能，而且为了治理平台，保护他人的版权，今日头条通过自查和用户举报等方式处理了多个侵权账号，打击侵权行动取得了明显的效果。这是今日头条为应对负外部效应所采取的自查行动，表明了平台整改的决心，也是一个平台长期发展壮大的觉悟。

- **收取准入费和收取增强型接入费用：** 今日头条主要的变现方式就是通过精准分发广告以及提供流量入口进行分成，分别属于收取准入费和收取增强型接入费用，根据今日头条自身的情况，这样的做法无可厚非，但变现方式还是太过单一，仍有很大的拓展空间。
- **平台生态系统：** 目前，今日头条已经拥有“火山小视频”、“西瓜视频”、“悟空问答”几大模块，这使得今日头条的用户群体发生了重大变化。“火山小视频”占得下沉市场；通过“悟空问答”聚合头部高知群体，提升高知流量与精品内容;通过“抖音”聚集潮流00 后，不断渗透和影响年轻化群体。但是大多还是局限在视频方面的产品，如果要扩大平台的影响力，就要重视拓展平台生态系统，衍生出除了信息推送以外的其他有效交互。
- **平台智能自我治理原则：** 在遵循英特尔架构实验室推出USB标准的自我治理规则中，“公平地对待所有人的知识产权”这一点今日头条做得并不够，从上面的侵权问题可以看得出来，而“随着平台公司的成熟，更多的决策会青睐于从核心平台走向外部拓展，从而对现有平台进行补充，讲那些有可能吞噬平台的新业务纳入麾下”这点，是今日头条正在为之努力的一个方向，各种新追加的模块以及今日头条的生态系统架构都可以证明这一点。

#### A-3 平台中国案例建议
- **注重反设计原则：** 在上文提及今日头条个性化推荐过度的情况，个人认为要为偶然和意外留下空间，可以通过版块中的分栏解决，除了“推荐”之外，随机追加标签，给用户除了个性化推荐之外的空间，强化反设计原则。
- **恰当的策展：** 为了应对负面的外部效应以及政策上的压力，今日头条应该进行更有效的策展，除了使用算法进行精准推荐之外，最好还要筛选和过滤不良和有害信息，严厉打击标题党，同时对内容设立推广等级制度，优质内容优先分发，低质内容减少其分发人数甚至不对其进行分发推广，涉嫌抄袭的头条号应采取封禁措施，禁止其发文同时保留对抄袭作者起诉的权利。通过设定相关制度严厉打击抄袭，内容生态加强净化机制。通过平台筛选让用户获得更好的匹配价值。
- **平台生态系统：** 除了以上列出的模块之外，为了完善今日头条的生态系统，需要拓展自身产品种类，丰富产品类型使之形成互补，并多方面布局内容类产业，完善自身产业链条，不能仅局限于新闻客户端的类型，需把控当前用户需求，及时布局，例如通过短视频领域，发现优秀作者，延伸相关类型 IP，未来进入图书、游戏或电影等行业；与视频类网站全面合作，达到引流作用，形成其独有内容矩阵；加强与用户的线下交流，开展相关活动推广其知名度，增强用户使用粘性。
- **拓展平台盈利化方式：** 除了收取准入费和收取增强型接入费用之外，还可以考虑优化自身，从而收取增强型内容管理服务费用，如增加更多优质资讯内容从而收取每月订阅费，而文章的生产者也可以获取提成作为工资。还有与网络小说网站进行合作，加大小说的推广力度，从小说的推广费用及打赏费用中获得抽成；与游戏厂商进行合作，多元化布局，游戏攻略、游戏直播、游戏商品购买等，从游戏消费中获得提成；同时应积极拓展线下盈利模式，与线下商铺进行有效合作，在本地新闻栏目及推荐栏目中进行推广，以点击量来计算费用等等方式。

### B-1 大数据理论
#### 数据废气
- 作为一个通过算法推荐系统向用户推荐感兴趣的内容分发平台，今日头条更加关注来自用户的交互，因此作为用户在线交互副产品的数据废气显得格外有价值。通过数据废气的收集和分析不断增加新的用户模型，原来的用户模型不用撤消，仍然发挥作用。在还没有推出头条号时，内容主要是抓取其它平台的文章，然后去重，一年几百万级，并不太大。主要是用户动作日志收集，兴趣收集，用户模型收集。资讯App的技术指标，比如屏幕滑动，用户是不是对一篇都看完，停留时间等都在特别关注。这也是今日头条能持续获得成功的原因之一。

#### B-2 大数据中国案例分析成果
- **数据独裁**： 上文也提及过个性化推荐过度的情况，这其实是一种数据独裁，过于依赖数据，这可能会带来决策的失误，比如将一个个用户的情报资讯、用户产生的数据废气和用户提供的反馈进行量化，从而生成个性化推荐，但是过度地注重这种量化的结果是产生了标题党，内容质量参差不齐等乱象，降低了用户体验，反而达不到预期的效果。
- **隐私风险**：近日，今日头条因为通讯录隐私窃取被用户告上法庭一案引起了诸多争议。在法庭上，今日头条却认为电话号码并不属于个人的隐私，是可以公开的，并且认为这些都是经过用户许可的，不属于侵犯隐私。这无疑是把今日头条再次推向舆论风波，对于这种打法律擦边球的行为，引起了用户的不满，也让不少使用该app的用户陷入隐私安全问题得不到保障的恐慌。今日头条一方认为他们事先告知了用户在同意使用今日头条app的同时，有权读取他们的通讯录信息，也有权限上传和储存，这些权限都是在一开始就获得了用户许可的，所以并不存在侵权行为，但我认为，“告知与许可”不应该成为平台公司窃取用户数据、随意使用用户数据的保护伞。互联网平台的负责人不应该只注重数据给他们带来的好处，也需要站在用户层面考虑问题，平台和用户是会相互影响的，平台被用户告上法庭，其实非常影响平台形象和信誉，更有可能让用户不再信任该平台，从而转去其他平台。

#### B-3 大数据中国案例建议
- **管理变革：** 

    a.在保护用户隐私方面，从个人许可到让数据使用者承担责任，数据使用者作为数据的收益方，理应为更加注重用户个人隐私的保护，履行好保护用户隐私的职责，除了大众监督还要加上法律手段，杜绝打法律擦边球的现象；

    b.保障内容质量，用户没有理由为这些质量低下的内容买单，当今日头条的推荐中出现了侵权、内容不符合规定时，国家版权局、广电总局等就可以对今日头条进行处罚，它需要为自己的过失承担责任。
- **大数据思维公司：** 今日头条的开发者并不是专业的新闻人，但是它在不断的发展中，应当使自己成为数据、技能和思维三者兼备的平台，从而增加今日头条的竞争力。
- **避免数据独裁**：灵活性才能提供关键竞争优势，平台发展到一定阶段可能会遇到瓶颈，决策者完全依赖数据是很愚蠢的。对此，决策者应该在数据的基础上更多地结合团队的智慧，使决策更具灵活性与未来洞察力，避免数据独裁，数据一言堂。

### C 大数据及平台两论点的整合
- **反设计原则与数据独裁：** 对于今日头条而言，它的过度个性化推荐体现出了这两个论点的问题，相反的，当过度个性化问题得到解决时，今日头条既强化了反设计原则，也削弱了数据独裁的问题。
- **恰当的策展与数据废气：** 今日头条如果想要优化策展，就应当更好地利用数据废气，过滤和筛选不良和抄袭的文章，更好地贴合用户需求，创造更大价值。
- **政策和管理变革：** 这都是来自国家政府的监管，对今日头条而言，更多的是侧重于国家版权局和广电总局的压力，今日头条想要走的更远，就需要承担好自己的责任，以免以前的罚款和约谈再次发生。